desc "This task is called by the Heroku scheduler add-on"


task :link_instrument => :environment do

		require "google/cloud/vision"

		Google::Cloud::Vision.configure do |config|
		  config.project_id  = "lottery-220502"
		  config.credentials = File.join(Rails.root, 'lottery-c12cc4ecd43a.json')
		end

		Instrument.all.each do |instrument|
		
			vision = Google::Cloud::Vision.new

			image = vision.image "https:"+instrument.scan.url

			document = image.document
			@text=document.text

			@error=''
			@emo_number_index=@text.index('EMO NUMBER')
			@dated_index=@text.index('DATED')
			
			if @emo_number_index==nil || @dated_index==nil
			  @error+=' emo number not detected'
			end
			
			if @error==''

				@emo_number=@text[(@emo_number_index+12)..(@dated_index-2)]
				money_receipt=MoneyReceipt.find_by_instrument_number(@emo_number)
				if money_receipt==nil
				else
					instrument.update(money_receipt_id: money_receipt.id)
				end
			end
		end

end

