class SetupController < ApplicationController

def index
end

#--------------------------------------------------------------------
def customer_index
  @header='Lotteries'
  @search_in_navbar = true
  @customers=Customer.all 
end

def customer_new
  @header='Create Customer'
  @customer=Customer.new
  @customer_action='customer_create'
end

def customer_create
  @customer= Customer.new(customer_params)
  if params[:customer][:mobile]!=''
  	if Customer.find_by_mobile(params[:customer][:mobile])==nil
  	@customer.save	
  	flash[:info]='Customer was successfully created.'
  	else
  	flash[:danger]='Customer not created, as same mobile no. detected'
  	end
  else
  @customer.save
  flash[:info]='Customer was successfully created.'
  end
  redirect_to setup_customer_index_url
end

def customer_edit
  @header='Edit customer' 
  @customer=Customer.find(params[:format])
  @customer_action='customer_update' 
end

def customer_update
  @customer=Customer.find(params[:customer_id])
  @customer.update(customer_params)
  flash[:info]='Customer was successfully updated.'
  redirect_to transaction_lottery_details_pending_url
end

def customer_destroy
  @customer=Customer.find(params[:format])
  if MoneyReceipt.find_by_customer_id(@customer.id)==nil
  @customer.destroy
  flash[:info]='Customer was successfully destroyed.'
  else
  flash[:danger]='Customer could not be destroyed, money receipt exists against the same'  
  end
  redirect_to setup_customer_index_url
end

#--------------------------------------------------------------------

def lottery_index
  @header='Lotteries'
  @search_in_navbar = true
  @lotteries=Lottery.all 
end

def lottery_new
  @header='Create Lottery'
  @lottery=Lottery.new
  @lottery_action='lottery_create'
end

def lottery_create
  @lottery= Lottery.new(lottery_params)
  @lottery.save
  flash[:info]='Lottery was successfully created.'
  redirect_to setup_lottery_index_url
end

def lottery_edit
  @header='Edit Lottery' 
  @lottery=Lottery.find(params[:format])
  @lottery_action='lottery_update' 
end

def lottery_update
  @lottery=Lottery.find(params[:lottery_id])
  @lottery.update(lottery_params)
  flash[:info]='Lottery was successfully updated.'
  redirect_to setup_lottery_index_url
end

def lottery_destroy
  @lottery=Lottery.find(params[:format])
  if Series.find_by_lottery_id(@lottery.id)==nil
  @lottery.destroy
  flash[:info]='Lottery was successfully destroyed.'
  else
  flash[:danger]='Lottery could not be destroyed, Series exists against the same'  
  end
  redirect_to setup_lottery_index_url
end

#--------------------------------------------------------------------

def series_index
  @header='Lotteries'
  @search_in_navbar = true
  @series=Series.all 
end

def series_new
  @lotteries=[]
  Lottery.where(drawn: nil).each do |lottery|
  	@lotteries+=[[lottery.name, lottery.id]]
  end
  Lottery.where(drawn: false).each do |lottery|
  	@lotteries+=[[lottery.name, lottery.id]]
  end
  @lotteries=@lotteries.sort

  @header='Create Series'
  @series=Series.new
  @series_action='series_create'
end

def series_create
  @series= Series.new(series_params)
  @series.save
  flash[:info]='Series was successfully created.'
  redirect_to setup_series_index_url
end

def series_edit
  @lotteries=[]
  Lottery.where.not(drawn: true).each do |lottery|
  	@lotteries+=[[lottery.name, lottery.id]]
  end
  @lotteries=@lotteries.sort
  
  @header='Edit Series' 
  @series=Series.find(params[:format])
  @series_action='series_update' 
end

def series_update
  @series=Series.find(params[:series_id])
  @series.update(series_params)
  flash[:info]='Series was successfully updated.'
  redirect_to setup_series_index_url
end

def series_destroy
  @series=Series.find(params[:format])
  if Series.find_by_series_id(@series.id)==nil
  @series.destroy
  flash[:info]='Series was successfully destroyed.'
  else
  flash[:danger]='Series could not be destroyed, sale exists against the same'  
  end
  redirect_to setup_series_index_url
end

def lottery_params
  params.require(:lottery).permit(:name,:rate,:drawn)
end

def customer_params
  params.require(:customer).permit(:name,:address,:pincode,:mobile,:email,:remarks)
end

def series_params
  params.require(:series).permit(:name,:lottery_id)
end

end
