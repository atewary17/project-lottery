class ReportsController < ApplicationController

def index
end

def money_receipt_register
	if params[:print]=="Print"
		redirect_to transaction_envelope_print_url :printables => params[:printables] 
  	else
		@header = 'Register'
		@search_in_navbar = true
		@from=(Date.today)
		@to=(Date.today)+1
	  	
	  	if params[:range] != nil
	    @from=params[:range][:from]
	    @to=params[:range][:to]
	  	end
		@money_receipts=MoneyReceipt.includes(:sold_lotteries, :customer).where('money_receipts.created_at >=  ? AND money_receipts.created_at < ? AND money_receipts.lottery_details_entered = ?',@from, @to, true)
	end
end
end
