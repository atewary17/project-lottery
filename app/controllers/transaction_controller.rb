class TransactionController < ApplicationController
def index
end

def money_receipt_entry_form
end

def money_receipt_entry
Customer.transaction do	

# create customers
# create money receipt
old_customer_ids=[]
params[:customer][:id].each_with_index do |customer_id, index|
	if customer_id==nil || customer_id==''
		new_customer=Customer.new
		new_customer.name=params[:customer_name][index]
		new_customer.mobile=params[:customer_mobile][index]	
		new_customer.address=params[:customer][:address][index]
		new_customer.pincode=params[:customer][:pincode][index]
		new_customer.email=params[:customer][:email][index]
		new_customer.remarks=params[:customer][:remarks][index]
		new_customer.save

		money_receipt=MoneyReceipt.new
		money_receipt.customer_id=new_customer.id
			if params['instrument_type_'+((index+1).to_s)]=='MO'
			money_receipt.mo=true
			money_receipt.instrument_number=params[:customer][:instrument_number][index]
			elsif params['instrument_type_'+((index+1).to_s)]=='DD'
			money_receipt.dd=true
			money_receipt.instrument_number=params[:customer][:instrument_number][index]
			elsif params['instrument_type_'+((index+1).to_s)]=='Cash'
			money_receipt.cash=true
			elsif params['instrument_type_'+((index+1).to_s)]=='PWT'
			money_receipt.pwt=true	
			end
		money_receipt.amount=params[:customer][:amount][index]
		money_receipt.save

	else
		if old_customer_ids.include? customer_id
		new_customer=Customer.new
		new_customer.name=params[:customer_name][index]
		new_customer.mobile=params[:customer_mobile][index]	
		new_customer.address=params[:customer][:address][index]
		new_customer.pincode=params[:customer][:pincode][index]
		new_customer.email=params[:customer][:email][index]
		new_customer.remarks=params[:customer][:remarks][index]
		new_customer.save
		
		money_receipt=MoneyReceipt.new
		money_receipt.customer_id=new_customer.id
			if params['instrument_type_'+((index+1).to_s)]=='MO'
			money_receipt.mo=true
			money_receipt.instrument_number=params[:customer][:instrument_number][index]
			elsif params['instrument_type_'+((index+1).to_s)]=='DD'
			money_receipt.dd=true
			money_receipt.instrument_number=params[:customer][:instrument_number][index]
			elsif params['instrument_type_'+((index+1).to_s)]=='Cash'
			money_receipt.cash=true
			elsif params['instrument_type_'+((index+1).to_s)]=='PWT'
			money_receipt.pwt=true	
			end
		money_receipt.amount=params[:customer][:amount][index]
		money_receipt.save

		else
		old_customer_ids+=[customer_id]
		
		money_receipt=MoneyReceipt.new
		money_receipt.customer_id=customer_id.to_i
			if params['instrument_type_'+((index+1).to_s)]=='MO'
			money_receipt.mo=true
			money_receipt.instrument_number=params[:customer][:instrument_number][index]
			elsif params['instrument_type_'+((index+1).to_s)]=='DD'
			money_receipt.dd=true
			money_receipt.instrument_number=params[:customer][:instrument_number][index]
			elsif params['instrument_type_'+((index+1).to_s)]=='Cash'
			money_receipt.cash=true
			elsif params['instrument_type_'+((index+1).to_s)]=='PWT'
			money_receipt.pwt=true	
			end
		money_receipt.amount=params[:customer][:amount][index]
		money_receipt.save

		end
	end

end
redirect_to transaction_lottery_details_pending_url   
end
end

def lottery_details_pending
	@lottery_details_pending=MoneyReceipt.where(lottery_details_entered: nil)
end

def lottery_details_entry_form
	if params[:print]=="Print"
	redirect_to transaction_envelope_print_url :printables => params[:printables]
	else	
		@customers=[]
		lottery_details_pending=MoneyReceipt.where(lottery_details_entered: nil)
			lottery_details_pending.each do |lottery_details_pending|
				if lottery_details_pending.customer.mobile==nil
				@customers+=[[lottery_details_pending.customer.name+'-'+lottery_details_pending.amount.to_s+'-'+lottery_details_pending.instrument_detail, lottery_details_pending.id]]
				else
				@customers+=[[lottery_details_pending.customer.name+'-'+lottery_details_pending.amount.to_s+'-'+lottery_details_pending.customer.mobile+'-'+lottery_details_pending.instrument_detail, lottery_details_pending.id]]
				end
			end
		@customer=@customers.sort
		if params[:money_receipt_picked]==nil
		else
		@money_receipt_picked=params[:money_receipt_picked].to_i
		end

		@lotteries=[]
		  Lottery.where(drawn: nil).each do |lottery|
		  	@lotteries+=[[lottery.name, lottery.id]]
		  end
		  Lottery.where(drawn: false).each do |lottery|
		  	@lotteries+=[[lottery.name, lottery.id]]
		  end
		@lotteries=@lotteries.sort
	end
end

def lottery_serial_input
@inputs=params[:inputs]
@old_lottery_ids=params[:old_lottery_ids]
@old_lottery_ids=@old_lottery_ids.split(' ')
@inputs=@inputs.split(',')

@lotteries_remove=[]
@old_lottery_ids.each do |old_lottery_id|
  if @inputs.include?(old_lottery_id)
  else
  @lotteries_remove+=[old_lottery_id]
  end
end
@lotteries_add=[]

@inputs.each do |input|
  if @old_lottery_ids.include?(input)
  else
  @lotteries_add+=[input]
  end
end

  respond_to do |format|
      format.js { render :action => "lottery_input_serial"}
  end
end

def serial_input_appear
@series_id=params[:series_id]
@quantity=params[:quantity]
	respond_to do |format|
	      format.js { render :action => "serial_input_appear"}
	end
end

def autocomplete_customer_name
  customer_name = params[:term]
  customer_name=customer_name.gsub(/\(/, '')
  customer_name=customer_name.gsub(/\)/, '')
  customer_name=customer_name.gsub(/\&/, '')  
  customer_name=customer_name.gsub(/\s+$/, '')
  customer_name=customer_name.gsub(/\:/, '')
  customer_name=(customer_name.gsub(/\s+/, '&'))+":*"
  customers = Customer.where("to_tsvector(name) @@ to_tsquery(:q)", q: customer_name)
  render :json => customers.map { |customer| {:id => customer.id, :label => customer.name+"-"+customer.mobile , :value => customer.name } }
end

def autocomplete_customer_mobile
	customer_mobile = params[:term]
	customer_mobile=customer_mobile.gsub(/\(/, '')
	customer_mobile=customer_mobile.gsub(/\)/, '')
	customer_mobile=customer_mobile.gsub(/\&/, '')  
	customer_mobile=customer_mobile.gsub(/\s+$/, '')
	customer_mobile=customer_mobile.gsub(/\:/, '')
	customer_mobile=(customer_mobile.gsub(/\s+/, '&'))+":*"
	customers = Customer.where("to_tsvector(mobile) @@ to_tsquery(:q)", q: customer_mobile)
	render :json => customers.map { |customer| {:id => customer.id, :label => customer.mobile+"-"+customer.name , :value => customer.mobile } }
end

def fetch_customer_details
customer_id=params[:customer_id].to_i
@line_item_id=params[:id]

if @line_item_id.include? 'mobile'
@hidden_div_id=@line_item_id.sub 'customer_mobile', 'customer_div'
@customer_name_div_id=@line_item_id.sub 'customer_mobile', 'customer_name_div'
@customer_name_id=@line_item_id.sub 'customer_mobile', 'customer_name'
@customer_address_div_id=@line_item_id.sub 'customer_mobile', 'customer_address_div'
@customer_pincode_div_id=@line_item_id.sub 'customer_mobile', 'customer_pincode_div'
@customer_email_div_id=@line_item_id.sub 'customer_mobile', 'customer_email_div'
@customer_remarks_div_id=@line_item_id.sub 'customer_mobile', 'customer_remarks_div'
else
@hidden_div_id=@line_item_id.sub 'customer_name', 'customer_div'
@customer_mobile_div_id=@line_item_id.sub 'customer_name', 'customer_mobile_div'
@customer_mobile_id=@line_item_id.sub 'customer_name', 'customer_mobile'
@customer_address_div_id=@line_item_id.sub 'customer_name', 'customer_address_div'
@customer_pincode_div_id=@line_item_id.sub 'customer_name', 'customer_pincode_div'
@customer_email_div_id=@line_item_id.sub 'customer_name', 'customer_email_div'
@customer_remarks_div_id=@line_item_id.sub 'customer_name', 'customer_remarks_div'
end

@customer=Customer.find(customer_id)
	respond_to do |format|
		format.js { render :action => "set_customer_details"}
	end
end


def lottery_details_entry
  Lottery.transaction do	

	money_receipt=MoneyReceipt.find(params[:money_receipt][:id])
	money_receipt.update(rp_number: params[:rp_number], lottery_details_entered: true)
	
	params[:sold][:lotteries].each do |lottery_id|
		if lottery_id!=''
			sold_lottery=SoldLottery.new
			sold_lottery.money_receipt_id=money_receipt.id
			sold_lottery.lottery_id=lottery_id
			sold_lottery.save
			sold_lottery.lottery.series.each do |series|
				if params[series.id.to_s+'_serials']!=nil
					sold_series=SoldSeries.new
					sold_series.sold_lottery_id=sold_lottery.id
					sold_series.series_id=series.id
					sold_series.save
					params[series.id.to_s+'_serials'].each do |serial|
					sold_number=SoldNumber.new
					sold_number.sold_series_id=sold_series.id
					sold_number.serial=serial
					sold_number.save
					end
				end
			end
		end
	end	
  
  
  flash[:info]='Lottery Details Entry Done'
  redirect_to transaction_money_receipt_review_url :money_receipt_id => money_receipt.id 
  end
end

def money_receipt_review
@money_receipt=MoneyReceipt.find(params[:money_receipt_id])
	if params[:ok]=='OK'
	redirect_to transaction_lottery_details_pending_url
	elsif params[:edit]=='EDIT'
	redirect_to transaction_money_receipt_edit_form_url :money_receipt_id => @money_receipt.id
	end
end

def envelope_print
@printables=params[:printables]
@printables.each do |money_receipt_id|
money_receipt=MoneyReceipt.find(money_receipt_id)
money_receipt.update(printed: true)
end
render :layout => false
end

def money_receipt_edit_form
	@customers=[]
	lottery_details_pending=MoneyReceipt.where(lottery_details_entered: nil)
		lottery_details_pending.each do |lottery_details_pending|
		if lottery_details_pending.customer.mobile==nil
		@customers+=[[lottery_details_pending.customer.name+'-'+lottery_details_pending.amount.to_s+'-'+lottery_details_pending.instrument_detail, lottery_details_pending.id]]
		else
		@customers+=[[lottery_details_pending.customer.name+'-'+lottery_details_pending.amount.to_s+'-'+lottery_details_pending.customer.mobile+'-'+lottery_details_pending.instrument_detail, lottery_details_pending.id]]
		end
	end
	@money_receipt=MoneyReceipt.find(params[:money_receipt_id])	
	if @money_receipt.customer.mobile==nil
	@customers+=[[@money_receipt.customer.name+'-'+@money_receipt.amount.to_s+'-'+@money_receipt.instrument_detail, @money_receipt.id]]	
	else
	@customers+=[[@money_receipt.customer.name+'-'+@money_receipt.amount.to_s+'-'+@money_receipt.customer.mobile+'-'+@money_receipt.instrument_detail, @money_receipt.id]]	
	end
	@customer=@customers.sort
	@money_receipt_picked=@money_receipt.id

	@lotteries_entered=[]
	@money_receipt.sold_lotteries.each do |sold_lottery|
		@lotteries_entered+=[sold_lottery.lottery_id]
	end

	@lotteries=[]
	  Lottery.where(drawn: nil).each do |lottery|
	  	@lotteries+=[[lottery.name, lottery.id]]
	  end
	  Lottery.where(drawn: false).each do |lottery|
	  	@lotteries+=[[lottery.name, lottery.id]]
	  end
	@lotteries=@lotteries.sort
end

def money_receipt_edit_entry
	  Lottery.transaction do	

		old_money_receipt=MoneyReceipt.find(params[:money_receipt_id])

		old_money_receipt.update(rp_number: nil, lottery_details_entered: nil)

		old_money_receipt.sold_lotteries.each do |sold_lottery|
			sold_lottery.sold_series.each do |sold_series|
				sold_series.sold_numbers.destroy_all
				sold_series.destroy
			end
		sold_lottery.destroy	
		end

		money_receipt=MoneyReceipt.find(params[:money_receipt][:id])
		money_receipt.update(rp_number: params[:rp_number], lottery_details_entered: true, instrument_number: params[:instrument_number], amount: params[:amount], mo: nil, dd: nil, cash: nil, pwt: nil)
		if params[:instrument_type]=='MO'
			money_receipt.update(mo: true)
		elsif params[:instrument_type]=='DD'
			money_receipt.update(dd: true)
		elsif params[:instrument_type]=='Cash'
			money_receipt.update(cash: true)
		elsif params[:instrument_type]=='PWT'
			money_receipt.update(pwt: true)
		end
			
			
							


		params[:sold][:lotteries].each do |lottery_id|
			if lottery_id!=''
				sold_lottery=SoldLottery.new
				sold_lottery.money_receipt_id=money_receipt.id
				sold_lottery.lottery_id=lottery_id
				sold_lottery.save
				sold_lottery.lottery.series.each do |series|
					if params[series.id.to_s+'_serials']!=nil
						sold_series=SoldSeries.new
						sold_series.sold_lottery_id=sold_lottery.id
						sold_series.series_id=series.id
						sold_series.save
						params[series.id.to_s+'_serials'].each do |serial|
						sold_number=SoldNumber.new
						sold_number.sold_series_id=sold_series.id
						sold_number.serial=serial
						sold_number.save
						end
					end
				end
			end
		end	
	  
	  
	  flash[:info]='Lottery Details Edited'
	  redirect_to transaction_money_receipt_review_url :money_receipt_id => money_receipt.id 
	  end
end

def upload_instrument
end

def instrument_upload
	@personnel=Personnel.find(1)
	if params[:instruments]
        params[:instruments].each { |instrument|
        @personnel.instruments.create(scan: instrument)
        }
    end
redirect_to :back
end

def instruments_uploaded
@instruments=Instrument.where(entry_done: nil).limit(10)
end

def emo_entry
Customer.transaction do
if params[:submit]=="Enter eMO Details"

	require "google/cloud/vision"

	Google::Cloud::Vision.configure do |config|
	  config.project_id  = "lottery-220502"
	  config.credentials = File.join(Rails.root, 'lottery-c12cc4ecd43a.json')
	end

	params[:instruments].each do |instrument_id|
	instrument=Instrument.find(instrument_id)
	
		vision = Google::Cloud::Vision.new

		image = vision.image "https:"+instrument.scan.url

		@name=''
		@address=''
		@mobile=''
		@pincode=''
		@amount=''
		@emo_number=''

		document = image.document
		@text=document.text

		@error=''
		@name_and_address_index=@text.index('Name and Address')
		@payee_index=@text.index('Pay')
		if @name_and_address_index==nil || @payee_index==nil
		  @error+=' name & address not detected'
		end
		@emo_number_index=@text.index('EMO NUMBER')
		@dated_index=@text.index('DATED')
		
		if @emo_number_index==nil || @dated_index==nil
		  @error+=' emo number not detected'
		end
		
		@rs_index=@text.index('Rs.')
		@words_index=@text.index('words)')

		if @rs_index==nil || @words_index==nil
		  @error+=' amount not detected'
		end

		if @error=='' && MoneyReceipt.find_by_instrument_number(@emo_number)==nil

		instrument.update(entry_done: true)

			@name_and_address=@text[(@name_and_address_index+17)..(@payee_index-1)]
			@pincode=@name_and_address.scan(/\s[\d]{6}\s/).first.try{|x| x.strip}
			@mobile=@name_and_address.scan(/\s[\d]{10}\s/).first.try{|x| x.strip}

			if @pincode!=nil
			@name_and_address[@pincode]=""
			end
			if @mobile!=nil
			@name_and_address[@mobile]=""
			end

			@name=@name_and_address.scan(/\A\S+\s\S+/).first.try{|x| x.strip}
			if @name.length<=4
			@name=@name_and_address.scan(/\A\S+\s\S+\s\S+/).first.try{|x| x.strip}  
			end

			@name_and_address[@name]=""
			@address=@name_and_address
			

			@emo_number=@text[(@emo_number_index+12)..(@dated_index-2)]
			@amount=@text[(@rs_index+3)..(@words_index-6)]

			if @mobile==nil || @mobile=="" || @mobile==" "
			else
			new_customer=Customer.find_by_mobile(@mobile)
			end
			if new_customer==nil
				new_customer=Customer.new
				new_customer.name=@name
				new_customer.mobile=@mobile	
				new_customer.address=@address
				new_customer.pincode=@pincode
				new_customer.email=''
				new_customer.remarks=''
				new_customer.save
			end

				money_receipt=MoneyReceipt.new
				money_receipt.customer_id=new_customer.id
					money_receipt.mo=true
					money_receipt.instrument_number=@emo_number
				money_receipt.amount=@amount.to_i
				money_receipt.save
				instrument.update(money_receipt_id: money_receipt.id)
		end
	end

elsif params[:submit]=="Remove"
	params[:instruments].each do |instrument_id|
	instrument=Instrument.find(instrument_id)
	instrument.update(entry_done: true)
	end
end	
redirect_to :back
end
end

def untagged_instruments
@instruments=Instrument.where(entry_done: true, money_receipt_id: nil).limit(20)
end

end
