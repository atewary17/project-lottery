class InstrumentsController < ApplicationController
def index
  @instruments = Instrument.order('created_at')
 end

 #New action for creating a new instrument
 def new
  @instrument = Instrument.new
 end

 #Create action ensures that submitted instrument gets created if it meets the requirements
 def create
  @instrument = Instrument.new(instrument_params)
  if @instrument.save
   flash[:notice] = "Successfully added new instrument!"
   redirect_to root_path
  else
   flash[:alert] = "Error adding new instrument!"
   render :new
  end
 end

 private

 #Permitted parameters when creating a instrument. This is used for security reasons.
 def instrument_params
  params.require(:instrument).permit(:title, :image)
 end

end
