class Lottery < ActiveRecord::Base
has_many :series
validates_uniqueness_of :name
has_many :sold_lotteries
end
