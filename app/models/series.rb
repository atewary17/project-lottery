class Series < ActiveRecord::Base
belongs_to :lottery
validates_uniqueness_of :name, :scope => :lottery_id
has_many :sold_series
end
