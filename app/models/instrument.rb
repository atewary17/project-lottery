class Instrument < ActiveRecord::Base
	belongs_to :personnel
	belongs_to :money_receipt
	has_attached_file :scan, :storage => :s3, :bucket => ENV['S3_BUCKET_NAME'], :s3_region => ENV['AWS_REGION'], :path => "instrument/scans/:id", :url => ":s3_domain_url"
    validates_attachment_size :scan, :in => 0.megabytes..10.megabytes
    do_not_validate_attachment_file_type :scan
end