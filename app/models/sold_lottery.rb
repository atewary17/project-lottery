class SoldLottery < ActiveRecord::Base
belongs_to :lottery
belongs_to :money_receipt
has_many :sold_series
end
