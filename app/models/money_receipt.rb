class MoneyReceipt < ActiveRecord::Base
has_many :sold_lotteries
belongs_to :customer
has_one :instrument

	def instrument_detail
	instrument=''
		if self.dd==true
			instrument+='DD-'
		elsif self.mo==true
			instrument+='MO-'
		elsif self.cash==true
			instrument+='Cash'
		elsif self.pwt==true
			instrument+='PWT'
		end

		if self.instrument_number==nil
		else
			instrument+=instrument_number
		end
	return instrument
	end

end
