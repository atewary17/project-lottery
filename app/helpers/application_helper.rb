module ApplicationHelper
	def flash_class(level)
    	return "alert alert-dismissible alert-"+level.to_s
	end
end
