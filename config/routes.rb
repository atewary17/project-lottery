Rails.application.routes.draw do
  resources :personnels

  get "password_resets/new"
  get 'current_user' => "personnels#current_user"
  get "sessions/new"
  get "reports/index"
  get "transaction/index"

  get "log_out" => 'sessions#destroy', :as => "log_out"
  get "log_in" => 'sessions#new', :as => "log_in"
  get "sign_up" => 'personnels#new', :as => "sign_up"
  root :to => 'personnels#new'
  resources :sessions
  resources :password_resets

# -----------------------------------------
  post 'setup/customer_index'
  get 'setup/customer_index'
  get 'setup/customer_new'
  post 'setup/customer_create'
  get 'setup/customer_edit'
  patch 'setup/customer_update'
  get 'setup/customer_destroy'

# -----------------------------------------
  get 'setup/index'
  post 'setup/lottery_index'
  get 'setup/lottery_index'
  get 'setup/lottery_new'
  post 'setup/lottery_create'
  get 'setup/lottery_edit'
  patch 'setup/lottery_update'
  get 'setup/lottery_destroy'

# -----------------------------------------
  post 'setup/series_index'
  get 'setup/series_index'
  get 'setup/series_new'
  post 'setup/series_create'
  get 'setup/series_edit'
  patch 'setup/series_update'
  get 'setup/series_destroy'
# -----------------------------------------

  get 'transaction/money_receipt_entry_form'
  post 'transaction/money_receipt_entry_form'
  get 'transaction/lottery_details_entry_form'
  post 'transaction/lottery_details_entry_form'
  get 'transaction/money_receipt_entry'
  post 'transaction/money_receipt_entry'
  get 'transaction/lottery_details_entry'
  post 'transaction/lottery_details_entry'
  get 'transaction/money_receipt_review'
  post 'transaction/money_receipt_review'
  get 'transaction/money_receipt_edit_form'
  post 'transaction/money_receipt_edit_form'
  get 'transaction/money_receipt_edit_entry'
  post 'transaction/money_receipt_edit_entry'
  get 'transaction/lottery_serial_input'
  post 'transaction/lottery_serial_input'
  get 'transaction/serial_input_appear'
  post 'transaction/serial_input_appear'
  get 'transaction/envelope_print'
  post 'transaction/envelope_print'
  post 'transaction/autocomplete_customer_name' => 'transaction#autocomplete_customer_name'
  get 'transaction/autocomplete_customer_name' => 'transaction#autocomplete_customer_name'
  post 'transaction/autocomplete_customer_mobile' => 'transaction#autocomplete_customer_mobile'
  get 'transaction/autocomplete_customer_mobile' => 'transaction#autocomplete_customer_mobile'
  get 'transaction/fetch_customer_details'
  post 'transaction/fetch_customer_details'
  get 'transaction/lottery_details_pending'
  post 'transaction/lottery_details_pending'
  get 'transaction/upload_instrument'
  post 'transaction/upload_instrument'
  get 'transaction/instrument_upload'
  post 'transaction/instrument_upload'
  get 'transaction/instruments_uploaded'
  post 'transaction/instruments_uploaded'
  get 'transaction/emo_entry'
  post 'transaction/emo_entry'
  get 'transaction/untagged_instruments'
  post 'transaction/untagged_instruments'
          

# -----------------------------------------
  post 'reports/money_receipt_register'
  get 'reports/money_receipt_register'


end
