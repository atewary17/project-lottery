# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181027022008) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "customers", force: :cascade do |t|
    t.text     "address"
    t.string   "email",      limit: 255
    t.text     "remarks"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",       limit: 255
    t.string   "mobile",     limit: 255
    t.string   "pincode",    limit: 255
  end

  create_table "instruments", force: :cascade do |t|
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "scan_file_name"
    t.string   "scan_content_type"
    t.integer  "scan_file_size"
    t.datetime "scan_updated_at"
    t.integer  "personnel_id"
    t.string   "error"
    t.boolean  "entry_done"
    t.integer  "money_receipt_id"
  end

  create_table "lotteries", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.decimal  "rate"
    t.boolean  "drawn"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "money_receipts", force: :cascade do |t|
    t.integer  "customer_id"
    t.boolean  "dd"
    t.boolean  "mo"
    t.string   "instrument_number",       limit: 255
    t.string   "rp_number",               limit: 255
    t.boolean  "printed"
    t.boolean  "smsed"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "amount"
    t.boolean  "cash"
    t.boolean  "pwt"
    t.boolean  "lottery_details_entered"
  end

  create_table "personnels", force: :cascade do |t|
    t.string   "email",                  limit: 255
    t.string   "name",                   limit: 255
    t.string   "passwordhash",           limit: 255
    t.string   "passwordsalt",           limit: 255
    t.string   "auth_token",             limit: 255
    t.string   "password_reset_token",   limit: 255
    t.datetime "password_reset_sent_at"
    t.string   "mobile",                 limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "series", force: :cascade do |t|
    t.integer  "lottery_id"
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sold_lotteries", force: :cascade do |t|
    t.integer  "money_receipt_id"
    t.integer  "lottery_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sold_numbers", force: :cascade do |t|
    t.integer  "sold_series_id"
    t.string   "serial",         limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sold_series", force: :cascade do |t|
    t.integer  "sold_lottery_id"
    t.integer  "series_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
