class CreateSeries < ActiveRecord::Migration
  def change
    create_table :series do |t|
      t.integer :lottery_id
      t.string :name

      t.timestamps
    end
  end
end
