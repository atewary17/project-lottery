class CreateSoldNumbers < ActiveRecord::Migration
  def change
    create_table :sold_numbers do |t|
      t.integer :sold_series_id
      t.string :serial

      t.timestamps
    end
  end
end
