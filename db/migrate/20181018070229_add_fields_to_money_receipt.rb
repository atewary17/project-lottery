class AddFieldsToMoneyReceipt < ActiveRecord::Migration
  def change
  	add_column :money_receipts, :amount, :decimal
  	add_column :money_receipts, :cash, :boolean
  	add_column :money_receipts, :pwt, :boolean
  	add_column :money_receipts, :lottery_details_entered, :boolean
  end
end
