class AddErrorToInstrument < ActiveRecord::Migration
  def change
    add_column :instruments, :error, :string
  end
end
