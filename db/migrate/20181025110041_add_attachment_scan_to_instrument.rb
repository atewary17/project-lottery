class AddAttachmentScanToInstrument < ActiveRecord::Migration
   def self.up
    change_table :instruments do |t|
      t.attachment :scan
    end
  end

  def self.down
    drop_attached_file :instruments, :scan
  end
end
