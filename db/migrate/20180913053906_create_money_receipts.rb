class CreateMoneyReceipts < ActiveRecord::Migration
  def change
    create_table :money_receipts do |t|
      t.integer :customer_id
      t.boolean :dd
      t.boolean :mo
      t.string :instrument_number
      t.string :rp_number
      t.boolean :printed
      t.boolean :smsed

      t.timestamps
    end
  end
end
