class CreateSoldLotteries < ActiveRecord::Migration
  def change
    create_table :sold_lotteries do |t|
      t.integer :money_receipt_id
      t.integer :lottery_id

      t.timestamps
    end
  end
end
