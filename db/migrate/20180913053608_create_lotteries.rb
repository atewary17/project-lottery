class CreateLotteries < ActiveRecord::Migration
  def change
    create_table :lotteries do |t|
      t.string :name
      t.decimal :rate
      t.boolean :drawn

      t.timestamps
    end
  end
end
