class AddEntryDoneToInstrument < ActiveRecord::Migration
  def change
    add_column :instruments, :entry_done, :boolean
  end
end
