class AddMoneyReceiptIdToInstrument < ActiveRecord::Migration
  def change
    add_column :instruments, :money_receipt_id, :integer
  end
end
