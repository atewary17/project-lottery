class CreateSoldSeries < ActiveRecord::Migration
  def change
    create_table :sold_series do |t|
      t.integer :sold_lottery_id
      t.integer :series_id

      t.timestamps
    end
  end
end
