class AddPersonnelIdToInstrument < ActiveRecord::Migration
  def change
    add_column :instruments, :personnel_id, :integer
  end
end
