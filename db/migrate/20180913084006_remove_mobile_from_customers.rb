class RemoveMobileFromCustomers < ActiveRecord::Migration
  def change
    remove_column :customers, :mobile, :integer
    remove_column :customers, :pincode, :integer
    add_column :customers, :mobile, :string
    add_column :customers, :pincode, :string
  end
end
