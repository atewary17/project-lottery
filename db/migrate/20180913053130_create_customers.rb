class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.text :address
      t.integer :pincode
      t.integer :mobile
      t.string :email
      t.text :remarks

      t.timestamps
    end
  end
end
